package com.example.myeconomy.mapper.economy;

import com.example.myeconomy.domain.economy.Category;
import com.example.myeconomy.dto.economy.CategoryCreateDto;
import com.example.myeconomy.dto.economy.CategoryResponseDto;
import com.example.myeconomy.dto.economy.CategoryUpdateDto;
import com.example.myeconomy.mapper.BaseMapper;
import com.example.myeconomy.repository.auth.IUserRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(
        componentModel = "spring",
        imports = {IUserRepository.class}
)
public abstract class CategoryMapper implements BaseMapper<Category, CategoryResponseDto, CategoryCreateDto, CategoryUpdateDto> {

    protected static IUserRepository userRepository;

    @Autowired
    public void setUserRepository(IUserRepository userRepository) {
        CategoryMapper.userRepository = userRepository;
    }

    @Override
    @Mapping(target = "user", expression = "java( userRepository.find(createDto.getUserId()).orElseThrow(" +
            "                () -> new com.example.myeconomy.exception.NotFoundException(" +
            "                                       com.example.myeconomy.utils.MessageService.getMessage(" +
            "                                               com.example.myeconomy.utils.MessageKeys.USER_NOT_FOUND))))")
    public abstract Category fromCreateDto(CategoryCreateDto createDto);

    @Override
    public abstract CategoryResponseDto toDto(Category entity);
}