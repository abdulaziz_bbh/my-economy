package com.example.myeconomy.mapper.economy;

import com.example.myeconomy.domain.economy.Finance;
import com.example.myeconomy.dto.economy.FinanceCreateDto;
import com.example.myeconomy.dto.economy.FinanceResponseDto;
import com.example.myeconomy.dto.economy.FinanceUpdateDto;
import com.example.myeconomy.mapper.BaseMapper;
import com.example.myeconomy.repository.auth.IUserRepository;
import com.example.myeconomy.repository.economy.ICategoryRepository;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(
        componentModel = "spring",
        imports = {IUserRepository.class, ICategoryRepository.class}
)
public abstract class FinanceMapper implements BaseMapper<Finance, FinanceResponseDto, FinanceCreateDto, FinanceUpdateDto> {

    protected static IUserRepository userRepository;
    protected static ICategoryRepository categoryRepository;

    @Autowired
    public void setUserRepositoryAndCategoryRepository(IUserRepository userRepository, ICategoryRepository categoryRepository){
        FinanceMapper.userRepository = userRepository;
        FinanceMapper.categoryRepository = categoryRepository;
    }


    @Override
    @Mapping(target = "user", expression = "java( userRepository.find(createDto.getUserId()).orElseThrow(" +
            "                () -> new com.example.myeconomy.exception.NotFoundException(" +
            "                                       com.example.myeconomy.utils.MessageService.getMessage(" +
            "                                               com.example.myeconomy.utils.MessageKeys.USER_NOT_FOUND))))")
    @Mapping(target = "category", expression = "java( categoryRepository.find(createDto.getCategoryId()).orElseThrow(" +
            "                () -> new com.example.myeconomy.exception.NotFoundException(" +
            "                                       com.example.myeconomy.utils.MessageService.getMessage(" +
            "                                               com.example.myeconomy.utils.MessageKeys.CATEGORY_N0T_FOUND))))")
    public abstract Finance fromCreateDto(FinanceCreateDto createDto);

}
