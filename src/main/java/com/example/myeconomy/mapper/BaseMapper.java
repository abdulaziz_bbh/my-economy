package com.example.myeconomy.mapper;

import com.example.myeconomy.domain.BaseDomain;
import com.example.myeconomy.dto.Dto;
import org.mapstruct.BeanMapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import java.util.List;

/**
 * @param <E> Entity
 * @param <D> Dto
 * @param <CR> CreateDto
 * @param <UP> UpdateDto
 */

public interface BaseMapper<E extends BaseDomain, D extends Dto, CR extends Dto, UP extends Dto>{

    D toDto(E entity);

    E fromDto(D dto);

    E fromCreateDto(CR createDto);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    E fromUpdateDto(UP updateDto, @MappingTarget E entity);

    List<D> toDto(List<E> entityList);

    List<E> fromToDto(List<D> dtoList);
}
