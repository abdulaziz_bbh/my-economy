package com.example.myeconomy.mapper.auth;

import com.example.myeconomy.domain.auth.User;
import com.example.myeconomy.dto.auth.UserCreateDto;
import com.example.myeconomy.dto.auth.UserDto;
import com.example.myeconomy.dto.auth.UserUpdateDto;
import com.example.myeconomy.mapper.BaseMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

@Mapper(
        componentModel = "spring",
        imports = {PasswordEncoder.class}
)
public abstract class UserMapper implements BaseMapper<User, UserDto, UserCreateDto, UserUpdateDto> {

    protected static PasswordEncoder passwordEncoder;

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        UserMapper.passwordEncoder = passwordEncoder;
    }

    @Override
    public abstract User fromDto(UserDto dto);



    @Override
    @Mapping(target = "password", expression = "java(passwordEncoder.encode(createDto.getPassword()))")
    @Mapping(target = "role", expression = "java(com.example.myeconomy.domain.auth.Role.USER)")
    public abstract User fromCreateDto(UserCreateDto createDto);

    @Override
    public abstract UserDto toDto(User user);
}
