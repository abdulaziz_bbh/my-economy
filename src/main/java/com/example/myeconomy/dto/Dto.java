package com.example.myeconomy.dto;

import java.io.Serializable;

/**
 * Bu interface implementatsiya qilgan class'lar DTO class ekanligini anglatadi
 */
public interface Dto extends Serializable {
}
