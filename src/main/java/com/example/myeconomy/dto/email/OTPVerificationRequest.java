package com.example.myeconomy.dto.email;

import com.example.myeconomy.dto.Dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OTPVerificationRequest implements Dto {

    private String email;
    private Integer otp;
}
