package com.example.myeconomy.dto.economy;

import com.example.myeconomy.dto.Dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * DTO for {@link com.example.myeconomy.domain.economy.Finance}
 */

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FinanceCreateDto implements Dto {

    @NotNull
    Double value;

    @NotNull
    String reason;

    @NotNull
    Long categoryId;

    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    LocalDate timeAt;

    @NotNull
    Long userId;

    @NotNull
    String incomeStatus;
}
