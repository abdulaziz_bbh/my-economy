package com.example.myeconomy.dto.economy;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DateBetweenRequest {

    @NotNull
    LocalDate startDate;

    @NotNull
    LocalDate endDate;
}
