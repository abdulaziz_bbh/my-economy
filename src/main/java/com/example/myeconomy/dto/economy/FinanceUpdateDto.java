package com.example.myeconomy.dto.economy;

import com.example.myeconomy.dto.Dto;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FinanceUpdateDto implements Dto {

    Double value;

    String reason;

    String incomeStatus;

    LocalDate timeAt;
}
