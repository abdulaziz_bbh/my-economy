package com.example.myeconomy.dto.economy;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SumOfIncomeCostResponse {

    @NotNull
    Double sum;

}
