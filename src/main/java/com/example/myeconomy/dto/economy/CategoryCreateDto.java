package com.example.myeconomy.dto.economy;

import com.example.myeconomy.dto.Dto;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO for {@link com.example.myeconomy.domain.economy.Category}
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryCreateDto implements Dto {

    @NotNull
    private String categoryName;

    @NotNull
    private String description;

    @NotNull
    private Long userId;

}
