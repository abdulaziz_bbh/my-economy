package com.example.myeconomy.dto.economy;

import com.example.myeconomy.enums.IncomeStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FinanceStatusResponse {

    @Enumerated(EnumType.STRING)
    IncomeStatus incomeStatus;

    @NotNull
    Double value;
}
