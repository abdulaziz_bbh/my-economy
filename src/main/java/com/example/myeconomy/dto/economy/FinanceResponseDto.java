package com.example.myeconomy.dto.economy;

import com.example.myeconomy.domain.auth.User;
import com.example.myeconomy.domain.economy.Category;
import com.example.myeconomy.dto.Dto;
import com.example.myeconomy.enums.IncomeStatus;
import com.example.myeconomy.enums.State;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * DTO for {@link com.example.myeconomy.domain.economy.Finance}
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class FinanceResponseDto implements Dto {

    Long id;

    Double value;

    String reason;

    LocalDate timeAt;

    Category category;

    User user;

    IncomeStatus incomeStatus;

    State state;

    LocalDateTime createdAt;

    LocalDateTime updatedAt;

    Long createdBy;

    Long updatedBy;
}
