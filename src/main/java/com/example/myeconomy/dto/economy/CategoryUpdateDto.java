package com.example.myeconomy.dto.economy;

import com.example.myeconomy.dto.Dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * DTO for {@link com.example.myeconomy.domain.economy.Category}
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryUpdateDto implements Dto {

    private String categoryName;

    private String description;
}
