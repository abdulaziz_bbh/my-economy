package com.example.myeconomy.dto.economy;

import com.example.myeconomy.domain.auth.User;
import com.example.myeconomy.dto.Dto;
import com.example.myeconomy.enums.State;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import java.time.LocalDateTime;

/**
 * DTO for {@link com.example.myeconomy.domain.economy.Category}
 */

@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryResponseDto implements Dto {

    Long id;

    String categoryName;

    String description;

    State state;

    LocalDateTime createdAt;

    LocalDateTime updatedAt;

    Long createdBy;

    Long updatedBy;

    User user;
}
