package com.example.myeconomy.dto.auth;

import com.example.myeconomy.dto.Dto;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AuthenticationDto implements Dto {

    private String accessToken;

    private String refreshToken;

}
