package com.example.myeconomy.dto.auth;

import com.example.myeconomy.domain.auth.Role;
import com.example.myeconomy.dto.Dto;
import com.example.myeconomy.enums.State;
import com.example.myeconomy.validation.ValidEmail;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

/**
 *  DTO for {@link com.example.myeconomy.domain.auth.User}
 */

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDto implements Dto {

    Long id;

    String email;

    String firstName;

    String lastName;

    Role role;

    State state;

    LocalDateTime createdAt;

    LocalDateTime updatedAt;

    Long createdBy;

    Long updatedBy;


}
