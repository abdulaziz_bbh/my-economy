package com.example.myeconomy.dto.auth;

import com.example.myeconomy.dto.Dto;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 *  DTO for {@link com.example.myeconomy.domain.auth.User}
 */
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserUpdateDto implements Dto {

    String firstName;

    String lastName;
}
