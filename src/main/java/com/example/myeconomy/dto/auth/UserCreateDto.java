package com.example.myeconomy.dto.auth;

import com.example.myeconomy.dto.Dto;
import com.example.myeconomy.utils.MessageService;
import com.example.myeconomy.validation.PasswordMatches;
import com.example.myeconomy.validation.ValidEmail;
import com.example.myeconomy.validation.ValidPassword;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

/**
 *  DTO for {@link com.example.myeconomy.domain.auth.User}
 */

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@PasswordMatches
public class UserCreateDto implements Dto {

    @NotNull
    @ValidEmail
    String email;

    @NotNull
    String firstName;

    @NotNull
    String lastName;

    @NotNull
    @ValidPassword
    String password;

    @NotNull
    String prePassword;

}


