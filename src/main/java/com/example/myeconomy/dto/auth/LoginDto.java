package com.example.myeconomy.dto.auth;

import com.example.myeconomy.dto.Dto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginDto implements Dto {

    private String username;

    private String password;
}
