package com.example.myeconomy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class MyEconomyApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyEconomyApplication.class, args);
    }

}
