package com.example.myeconomy.controller.auth;

import com.example.myeconomy.dto.auth.UserDto;
import com.example.myeconomy.dto.auth.UserUpdateDto;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.service.user.IUserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/user")
public class UserController {

    private final IUserService userService;

    @PutMapping("user-update/{id}")
    public ResponseData<UserDto> updateUser(@Valid @PathVariable("id") Long id, @RequestBody UserUpdateDto updateDto){
        return userService.update(id, updateDto);
    }

    @DeleteMapping("user-delete/{id}")
    public ResponseData<UserDto> deleteUser(@Valid @PathVariable("id") Long id){
        return userService.delete(id);
    }

}
