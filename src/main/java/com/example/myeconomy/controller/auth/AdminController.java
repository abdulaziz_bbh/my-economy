package com.example.myeconomy.controller.auth;

import com.example.myeconomy.dto.auth.UserDto;
import com.example.myeconomy.dto.economy.CategoryResponseDto;
import com.example.myeconomy.dto.economy.FinanceResponseDto;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.service.economy.ICategoryService;
import com.example.myeconomy.service.economy.IFinanceService;
import com.example.myeconomy.service.user.IUserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("api/v1/admin")
@RequiredArgsConstructor
public class AdminController {

    private final IUserService userService;
    private final ICategoryService categoryService;
    private final IFinanceService financeService;

    @GetMapping("get-user-by-username/{username}")
    public ResponseData<UserDto> getUserByUsername(@Valid @PathVariable("username")String username){
        return userService.findByUsername(username);
    }

    @GetMapping("user-find-by-id/{id}")
    public ResponseData<UserDto> findUserById(@Valid @PathVariable("id") Long id){
        return userService.getById(id);
    }

    @GetMapping("user-find-all")
    public ResponseData<List<UserDto>> findUserAll(){
        return userService.getAll();
    }

    @GetMapping("category/get-all")
    public ResponseData<List<CategoryResponseDto>> getAllCategory(){
        return categoryService.getAll();
    }

    @GetMapping("category/get-by-id/{id}")
    public ResponseData<CategoryResponseDto> getByIdCategory(@Valid @PathVariable("id") Long id){
        return categoryService.getById(id);
    }

    @GetMapping("finance/get-by-id/{id}")
    public ResponseData<FinanceResponseDto> getByIdFinance(@Valid @PathVariable("id") Long id){
        return financeService.getById(id);
    }

    @GetMapping("finance/get-all")
    public ResponseData<List<FinanceResponseDto>> getAllFinance(){
        return financeService.getAll();
    }

}
