package com.example.myeconomy.controller.auth;

import com.example.myeconomy.dto.auth.*;
import com.example.myeconomy.dto.email.OTPVerificationRequest;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.service.email.IOTPVerificationService;
import com.example.myeconomy.service.user.IAuthenticationService;
import com.example.myeconomy.service.user.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;

@RestController
@RequestMapping("api/v1/auth")
@RequiredArgsConstructor
public class AuthController {

    private final IUserService userService;
    private final IAuthenticationService authenticationService;
    private final IOTPVerificationService otpVerificationService;

    @Operation(
            description = "Post endpoint for user registration",
            summary = "Users register through this api",
            responses = {
                    @ApiResponse(
                            description = "If successful  user data is returned",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "An error message is returned if unsuccessful",
                            responseCode = "500/404"
                    )
            }
    )
    @PostMapping("sign-up")
    public ResponseData<UserDto> signUp(@Valid @RequestBody UserCreateDto createDto){
        return userService.create(createDto);
    }

    @Operation(
            description = "Post endpoint for user authentication/login",
            summary = "Users logs/authenticates through this api",
            responses = {
                    @ApiResponse(
                            description = "If successful  access token and refresh token is returned",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "An error message is returned if unsuccessful",
                            responseCode = "500/404"
                    )
            }
    )
    @PostMapping("sign-In")
    public ResponseData<AuthenticationDto> signIn(@Valid @RequestBody LoginDto loginDto){
        return authenticationService.login(loginDto);
    }



    @Operation(
            description = "Post endpoint for sending one time password to user's email",
            summary = "Sending otp to user's email through this api",
            responses = {
                    @ApiResponse(
                            description = "If successful  user data is returned",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "An error message is returned if unsuccessful",
                            responseCode = "500/404"
                    )
            }
    )
    @PostMapping("sending-otp/{email}")
    public ResponseData<String> sendingOTP(@Valid @PathVariable String email){
        return otpVerificationService.sendOTP(email);
    }

    @Operation(
            description = "Post endpoint for verification one time password",
            summary = "Verification one time password through this api",
            responses = {
                    @ApiResponse(
                            description = "If successful  user data is returned",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "An error message is returned if unsuccessful",
                            responseCode = "500/404"
                    )
            }
    )
    @PostMapping("verification-otp")
    public ResponseData<Boolean> verificationOTP(@Valid @RequestBody OTPVerificationRequest request){
        return otpVerificationService.verificationOTP(request);
    }

    @Operation(
            description = "Access Token eskirgan taqdirda token refresh qilish uchun api",
            summary = "Ushbu api orqali token expired bolganda refresh token yordamida tokenni refresh qilish mumkin",
            responses = {
                    @ApiResponse(
                            description = "Agar operatisiya muvoffaqiyatli bo'lsa access va refresh token qaytadi",
                            responseCode = "200"
                    ),
                    @ApiResponse(
                            description = "Agar operatsiya muvoffaqiyatsiz bo'lsa xatolik haqida xabar qaytadi",
                            responseCode = "500/404"
                    )
            }
    )
    @GetMapping("refresh-token")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        authenticationService.refreshToken(request, response);
    }

}
