package com.example.myeconomy.controller.economy;

import com.example.myeconomy.dto.economy.*;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.service.economy.IFinanceService;
import jakarta.validation.Valid;
import jakarta.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("api/v1/finance")
@RequiredArgsConstructor
public class FinanceController {

    private final IFinanceService financeService;

    @PostMapping("create")
    public ResponseData<FinanceResponseDto> create(@Valid @RequestBody FinanceCreateDto createDto){
        return financeService.create(createDto);
    }

    @PutMapping("update/{id}")
    public ResponseData<FinanceResponseDto> update(
            @Valid @PathVariable("id") Long id,
            @Valid @RequestBody FinanceUpdateDto updateDto){
        return financeService.update(id, updateDto);
    }

    @DeleteMapping("delete/{id}")
    public ResponseData<FinanceResponseDto> delete(@Valid @PathVariable("id") Long id){
        return financeService.delete(id);
    }

    @GetMapping("sum-of-cost-between/{id}")
    public ResponseData<SumOfIncomeCostResponse> getSumOfCostBetweenDate(
            @Valid @RequestBody DateBetweenRequest request,
            @Valid @PathVariable("id") Long id){
        return financeService.sumOfCostBetween(request, id);
    }

    @GetMapping("sum-of-income-between/{id}")
    public ResponseData<SumOfIncomeCostResponse> getSumOfIncomeBetweenDate(
            @Valid @RequestBody DateBetweenRequest request,
            @Valid @PathVariable("id") Long id){
        return financeService.sumOfIncomeBetween(request, id);
    }

    @GetMapping("sum-of-cost-month")
    public ResponseData<SumOfIncomeCostResponse> getSumOfCostMonth(
            @Valid @PathParam("month") Integer month,
            @Valid @PathParam("year") Integer year,
            @Valid @PathParam("userId") Long userId){
        return financeService.sumOfCostOnMonth(month, year, userId);
    }

    @GetMapping("sum-of-income-month")
    public ResponseData<SumOfIncomeCostResponse> getSumOfIncomeMonth(
            @Valid @PathParam("month") Integer month,
            @Valid @PathParam("year") Integer year,
            @Valid @PathParam("userId") Long userId){
        return financeService.sumOfIncomeOnMonth(month, year, userId);
    }

    @GetMapping("finance-of-cost-day")
    public ResponseData<List<FinanceResponseDto>> financeOfCostDay(
            @Valid @PathParam("date")LocalDate date,
            @Valid @PathParam("id") Long id){
        return financeService.financeOfCostDay(date, id);
    }

    @GetMapping("finance-of-income-day")
    public ResponseData<List<FinanceResponseDto>> financeOfIncomeDay(
            @Valid @PathParam("date") LocalDate date,
            @Valid @PathParam("id") Long id){
        return financeService.financeOfIncomeDay(date, id);
    }

    @GetMapping("get-status-month")
    public ResponseData<FinanceStatusResponse> getStatusMonth(
            @Valid @PathParam("month") Integer month,
            @Valid @PathParam("year") Integer year,
            @Valid @PathParam("id") Long id){
        return financeService.financeStatusOfMonth(year, month, id);
    }

    @GetMapping("get-status-day")
    public ResponseData<FinanceStatusResponse> getStatusDay(
            @Valid @PathParam("date")LocalDate date,
            @Valid @PathParam("id") Long id){
        return financeService.financeStatusOfDay(date, id);
    }
}
