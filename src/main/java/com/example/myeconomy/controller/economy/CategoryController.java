package com.example.myeconomy.controller.economy;

import com.example.myeconomy.dto.economy.CategoryCreateDto;
import com.example.myeconomy.dto.economy.CategoryResponseDto;
import com.example.myeconomy.dto.economy.CategoryUpdateDto;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.service.economy.ICategoryService;
import jakarta.validation.Valid;
import jakarta.websocket.server.PathParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/category")
@RequiredArgsConstructor
public class CategoryController {

    private final ICategoryService categoryService;

    @PostMapping("/create")
    public ResponseData<CategoryResponseDto> create(@Valid @RequestBody CategoryCreateDto createDto){
        return categoryService.create(createDto);
    }

    @PutMapping("/update/{id}")
    public ResponseData<CategoryResponseDto> update(@RequestBody CategoryUpdateDto updateDto, @Valid @PathVariable("id") Long id){
        return categoryService.update(id, updateDto);
    }

    @GetMapping("get-by-name")
    public ResponseData<CategoryResponseDto> getByName(@Valid @PathParam("name") String name, @Valid @PathParam("userId") Long userId){
        return categoryService.getByName(name, userId);
    }

    @GetMapping("get-all/{id}")
    public ResponseData<List<CategoryResponseDto>> getAllByUserId(@Valid @PathVariable("id") Long id){
        return categoryService.getAllByUserId(id);
    }

    @DeleteMapping("delete/{id}")
    public ResponseData<CategoryResponseDto> delete(@Valid @PathVariable("id") Long id){
        return categoryService.delete(id);
    }
}
