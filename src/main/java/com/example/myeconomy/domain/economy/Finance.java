package com.example.myeconomy.domain.economy;

import com.example.myeconomy.domain.BaseDomain;
import com.example.myeconomy.domain.auth.User;
import com.example.myeconomy.enums.IncomeStatus;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.cglib.core.Local;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Finance extends BaseDomain {

    @Column(nullable = false)
    Double value;

    @Column(nullable = false)
    String reason;

    @Column(nullable = false)
    LocalDate timeAt;

    @ManyToOne(cascade = CascadeType.ALL)
    Category category;

    @ManyToOne(cascade = CascadeType.ALL)
    User user;

    @Enumerated(EnumType.STRING)
    IncomeStatus incomeStatus;



}
