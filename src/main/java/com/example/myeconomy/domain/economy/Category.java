package com.example.myeconomy.domain.economy;
import com.example.myeconomy.domain.BaseDomain;
import com.example.myeconomy.domain.auth.User;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Category extends BaseDomain {

    @Column(nullable = false)
    String categoryName;

    String description;

    @ManyToOne(cascade = CascadeType.ALL)
    User user;

}
