package com.example.myeconomy.enums;

public enum State {
    NEW,
    UPDATED,
    DELETED,
    NONE
}
