package com.example.myeconomy.service.email;

public interface OtpProvider {

    Integer generatorOTP(String key);

    Integer getOTPByKey(String key);

    void removeOTPFromCache(String key);
}
