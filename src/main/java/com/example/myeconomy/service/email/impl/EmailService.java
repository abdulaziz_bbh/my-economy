package com.example.myeconomy.service.email.impl;

import com.example.myeconomy.dto.email.MessageToEmail;
import com.example.myeconomy.service.email.IEmailService;
import com.example.myeconomy.utils.MessageKeys;
import com.example.myeconomy.utils.MessageService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmailService implements IEmailService {

    private final Logger LOGGER = LoggerFactory.getLogger(EmailService.class);
    private final JavaMailSender javaMailSender;


    @Override
    public void sendMessage(MessageToEmail messageToEmail) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(String.join(",", messageToEmail.getRecipients()));
        message.setSubject(messageToEmail.getSubject());
        message.setText(messageToEmail.getBody());
        try {
            javaMailSender.send(message);
        } catch (MailSendException e) {
            LOGGER.error("Sending email error {}", e.getMessage());
            throw new MailSendException(MessageService.getMessage(MessageKeys.SENDING_EMAIL_ERROR));
        }
    }
}
