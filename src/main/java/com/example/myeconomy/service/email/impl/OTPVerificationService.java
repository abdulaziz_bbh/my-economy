package com.example.myeconomy.service.email.impl;

import com.example.myeconomy.domain.auth.User;
import com.example.myeconomy.dto.email.MessageToEmail;
import com.example.myeconomy.dto.email.OTPVerificationRequest;
import com.example.myeconomy.exception.NotFoundException;
import com.example.myeconomy.exception.OTPException;
import com.example.myeconomy.repository.auth.IUserRepository;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.service.email.IEmailService;
import com.example.myeconomy.service.email.IOTPVerificationService;
import com.example.myeconomy.service.email.OtpProvider;
import com.example.myeconomy.utils.MessageKeys;
import com.example.myeconomy.utils.MessageService;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class OTPVerificationService implements IOTPVerificationService{

    private final Logger LOGGER = LoggerFactory.getLogger(OTPVerificationService.class);
    private final IUserRepository userRepository;
    private final OtpProvider otpProvider;
    private final IEmailService emailService;

    @Override
    @Transactional
    public ResponseData<String> sendOTP(@NotNull String email) {
            sendingGeneratedOTP(email);
            return ResponseData.successResponse(MessageService.getMessage(MessageKeys.SENDING_OTP_SUCCESSFUL));
    }
    @Override
    @Transactional
    public ResponseData<Boolean> verificationOTP(OTPVerificationRequest request) {
        return new ResponseData<>(validateOTP(request.getEmail(), request.getOtp()));
    }
    private void sendingGeneratedOTP(String key){
        Integer optValue = otpProvider.generatorOTP(key);
        if (optValue == -1){
            LOGGER.error("OTP generator not working...");
            throw OTPException.restThrow(MessageService.getMessage(MessageKeys.OTP_GENERATOR_NOT_WORKING));
        }
        LOGGER.info("One time password: {}", optValue);

        User user = userRepository.findByEmail(key).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(MessageKeys.USER_NOT_FOUND)));
        List<String> recipients = new ArrayList<>();
        recipients.add(user.getUsername());

        MessageToEmail message = MessageToEmail.builder()
                .subject(MessageService.getMessage(MessageKeys.OTP_FOR_VERIFICATION_EMAIL))
                .body("One time password: "+ optValue)
                .recipients(recipients)
                .build();
        emailService.sendMessage(message);
    }
    private boolean validateOTP(String key, Integer otp){
        Integer otpFromOTP = otpProvider.getOTPByKey(key);
        User user = userRepository.findByEmail(key).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(MessageKeys.USER_NOT_FOUND)));
        if (otpFromOTP != null && otpFromOTP.equals(otp)){
            otpProvider.removeOTPFromCache(user.getUsername());
            user.setEnabled(true);
            userRepository.save(user);
            LOGGER.info("OTP matches");
            return true;
        }
        throw OTPException.restThrow(MessageService.getMessage(MessageKeys.OTP_NOT_MATCHED));
    }
}
