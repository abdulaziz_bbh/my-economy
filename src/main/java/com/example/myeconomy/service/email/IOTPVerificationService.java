package com.example.myeconomy.service.email;

import com.example.myeconomy.dto.email.OTPVerificationRequest;
import com.example.myeconomy.response.ResponseData;

public interface IOTPVerificationService {

    ResponseData<String> sendOTP(String email);

    ResponseData<Boolean> verificationOTP(OTPVerificationRequest request);
}
