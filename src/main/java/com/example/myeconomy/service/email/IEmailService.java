package com.example.myeconomy.service.email;

import com.example.myeconomy.dto.email.MessageToEmail;

public interface IEmailService {

    void sendMessage(MessageToEmail message);
}
