package com.example.myeconomy.service;

import com.example.myeconomy.dto.Dto;
import com.example.myeconomy.response.ResponseData;

import java.io.Serializable;
import java.util.List;

public interface GenericService <
        D extends Dto,
        CR extends Dto,
        UP extends Dto,
        ID extends Serializable>{

    ResponseData<D> getById(ID id);

    ResponseData<List<D>> getAll();

    ResponseData<D> create(CR createDto);

    ResponseData<D> update(ID id, UP updateDto);

    ResponseData<D> delete(ID id);
}
