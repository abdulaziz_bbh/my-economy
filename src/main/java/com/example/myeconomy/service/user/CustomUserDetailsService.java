package com.example.myeconomy.service.user;

import com.example.myeconomy.repository.auth.IUserRepository;
import com.example.myeconomy.utils.MessageKeys;
import com.example.myeconomy.utils.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username.isBlank() || username.isEmpty()){
            throw new NullPointerException(MessageService.getMessage(MessageKeys.THIS_FIELD_CANNOT_BE_NULL));
        }
        return userRepository.findByEmail(username).orElseThrow(
                () -> new UsernameNotFoundException(MessageService.getMessage(MessageKeys.USER_NOT_FOUND)));
    }
}
