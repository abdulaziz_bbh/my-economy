package com.example.myeconomy.service.user.impl;

import com.example.myeconomy.domain.auth.User;
import com.example.myeconomy.dto.auth.UserCreateDto;
import com.example.myeconomy.dto.auth.UserDto;
import com.example.myeconomy.dto.auth.UserUpdateDto;
import com.example.myeconomy.exception.AlreadyExistException;
import com.example.myeconomy.exception.NotFoundException;
import com.example.myeconomy.mapper.auth.UserMapper;
import com.example.myeconomy.repository.auth.IUserRepository;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.service.user.IUserService;
import com.example.myeconomy.utils.MessageKeys;
import com.example.myeconomy.utils.MessageService;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Locale;

import static com.example.myeconomy.utils.MessageKeys.*;

@Service
@Transactional
@RequiredArgsConstructor
public class UserService implements IUserService {

    private final IUserRepository userRepository;
    private final MessageSource messageSource;
    private final UserMapper userMapper;

    @Override
    public ResponseData<UserDto> getById(Long id) {
        User user = userRepository.find(id).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(USER_NOT_FOUND)));
        return ResponseData.successResponse(userMapper.toDto(user));
    }
    @Override
    public ResponseData<List<UserDto>> getAll() {
        List<User> users = userRepository.findAll();
        if (users.isEmpty()){
            throw new NotFoundException(messageSource.getMessage(
                    MessageKeys.NO_CONTENT,
                    null,
                    Locale.getDefault()
            ));
        }
        return ResponseData.successResponse(userMapper.toDto(users));
    }
    @Override
    public ResponseData<UserDto> create(UserCreateDto createDto) {
        existByEmail(createDto.getEmail());
        User user = userMapper.fromCreateDto(createDto);
        userRepository.save(user);
        return ResponseData.successResponse(userMapper.toDto(user));
    }
    @Override
    public ResponseData<UserDto> update(@NotNull Long id, UserUpdateDto updateDto) {
        User user = userRepository.find(id).orElseThrow(
                () -> new NotFoundException(messageSource.getMessage(
                        MessageKeys.USER_NOT_FOUND,
                        null,
                        Locale.getDefault())));
        user = userMapper.fromUpdateDto(updateDto, user);
        userRepository.update(user);
        return ResponseData.successResponse(userMapper.toDto(user));
    }
    @Override
    public ResponseData<UserDto> delete(@NotNull Long id) {
        User user = userRepository.find(id).orElseThrow(
                () -> new NotFoundException(messageSource.getMessage(
                        MessageKeys.USER_NOT_FOUND,
                        null,
                        Locale.getDefault())));
        userRepository.delete(user);
        return ResponseData.successResponse(userMapper.toDto(user));
    }
    @Override
    public void existByEmail(String email) {
        if (userRepository.existByEmail(email)) {
            throw new AlreadyExistException(messageSource.getMessage(USER_ALREADY_REGISTERED, null, Locale.getDefault()));

        }
    }
    @Override
    public ResponseData<UserDto> findByUsername(@NotNull String username) {
        User user = userRepository.findByEmail(username)
                .orElseThrow(() -> new NotFoundException(MessageService.getMessage(USER_NOT_FOUND)));
        return ResponseData.successResponse(userMapper.toDto(user));

    }
}