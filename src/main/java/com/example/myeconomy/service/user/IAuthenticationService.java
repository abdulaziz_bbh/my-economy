package com.example.myeconomy.service.user;

import com.example.myeconomy.dto.auth.AuthenticationDto;
import com.example.myeconomy.dto.auth.LoginDto;
import com.example.myeconomy.dto.auth.UserCreateDto;
import com.example.myeconomy.dto.auth.UserDto;
import com.example.myeconomy.response.ResponseData;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;

public interface IAuthenticationService {

    ResponseData<AuthenticationDto> login(LoginDto loginDto);

    void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException;
}
