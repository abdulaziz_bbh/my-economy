package com.example.myeconomy.service.user;

import com.example.myeconomy.dto.auth.UserCreateDto;
import com.example.myeconomy.dto.auth.UserDto;
import com.example.myeconomy.dto.auth.UserUpdateDto;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.service.GenericService;

public interface IUserService extends GenericService<
        UserDto,
        UserCreateDto,
        UserUpdateDto,
        Long> {

    void existByEmail(String email);

    ResponseData<UserDto> findByUsername(String username);

}
