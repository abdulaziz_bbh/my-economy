package com.example.myeconomy.service.user.impl;

import com.example.myeconomy.domain.auth.Token;
import com.example.myeconomy.domain.auth.User;
import com.example.myeconomy.dto.auth.AuthenticationDto;
import com.example.myeconomy.dto.auth.LoginDto;
import com.example.myeconomy.enums.State;
import com.example.myeconomy.exception.NotFoundException;
import com.example.myeconomy.repository.auth.IUserRepository;
import com.example.myeconomy.repository.auth.TokenRepository;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.security.JwtProvider;
import com.example.myeconomy.service.user.IAuthenticationService;
import com.example.myeconomy.utils.MessageKeys;
import com.example.myeconomy.utils.MessageService;
import com.example.myeconomy.utils.RestConstant;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthenticationService implements IAuthenticationService {

    private final IUserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final JwtProvider jwtProvider;
    private final TokenRepository tokenRepository;

    @Override
    public ResponseData<AuthenticationDto> login(LoginDto loginDto) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginDto.getUsername(), loginDto.getPassword()));

        User user = userRepository.findByEmail(loginDto.getUsername()).orElseThrow(
                () -> new UsernameNotFoundException(MessageService.getMessage(MessageKeys.USER_NOT_FOUND)));
        String accessToken = jwtProvider.generateAccessToken(user);
        String refreshToken = jwtProvider.generateRefreshToken(user);
        revokeAllUserToken(user);
        saveUserToken(user, accessToken);
        AuthenticationDto authenticationResponse = AuthenticationDto.builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();
        return ResponseData.successResponse(authenticationResponse);
    }

    @Override
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        final String refreshToken;
        final String userEmail;
        if (authHeader == null || !authHeader.startsWith(RestConstant.TOKEN_TYPE)){
            return;
        }
        refreshToken = authHeader.substring(7);
        userEmail = jwtProvider.extractUsername(refreshToken);
        if (userEmail != null){
            User user = this.userRepository.findByEmail(userEmail).orElseThrow(
                    () -> new NotFoundException(MessageService.getMessage(MessageKeys.USER_NOT_FOUND)));
            if (jwtProvider.isTokenValid(refreshToken, user)){
                String accessToken = jwtProvider.generateAccessToken(user);
                revokeAllUserToken(user);
                saveUserToken(user, accessToken);
                AuthenticationDto authenticationResponse = AuthenticationDto.builder()
                        .accessToken(accessToken)
                        .refreshToken(refreshToken)
                        .build();
                new ObjectMapper().writeValue(response.getOutputStream(), authenticationResponse);
            }
        }
    }

    private void revokeAllUserToken(User user){
        List<Token> tokens = tokenRepository.findAllValidTokenByUser(user.getId());
        if (tokens.isEmpty()) return;
        tokens.forEach(token -> {
            token.setRevoked(true);
            token.setExpired(true);
        });
        tokenRepository.saveAll(tokens);
    }
    private void saveUserToken(User user, String jwtToken){
        Token token = Token.builder()
                .user(user)
                .tokenType(RestConstant.TOKEN_TYPE)
                .token(jwtToken)
                .expired(false)
                .revoked(false)
                .state(State.NONE)
                .build();
        tokenRepository.save(token);
    }


}
