package com.example.myeconomy.service.economy.impl;

import com.example.myeconomy.domain.economy.Finance;
import com.example.myeconomy.dto.economy.*;
import com.example.myeconomy.enums.IncomeStatus;
import com.example.myeconomy.exception.NotFoundException;
import com.example.myeconomy.mapper.economy.FinanceMapper;
import com.example.myeconomy.repository.economy.IFinanceRepository;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.service.economy.IFinanceService;
import com.example.myeconomy.utils.MessageKeys;
import com.example.myeconomy.utils.MessageService;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class FinanceService implements IFinanceService {

    private final IFinanceRepository financeRepository;
    private final FinanceMapper financeMapper;


    @Override
    public ResponseData<FinanceResponseDto> getById(@NotNull Long id) {
        Finance finance = financeRepository.find(id).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(MessageKeys.FINANCE_NOT_FOUND)));
        return ResponseData.successResponse(financeMapper.toDto(finance));
    }
    @Override
    public ResponseData<List<FinanceResponseDto>> getAll() {
        List<Finance> financeList = financeRepository.findAll();
        if (financeList.isEmpty()){
            throw new NotFoundException(MessageService.getMessage(MessageKeys.NOT_FOUND_ANY_FINANCE));
        }
        return ResponseData.successResponse(financeMapper.toDto(financeList));
    }

    @Override
    public ResponseData<FinanceResponseDto> create(FinanceCreateDto createDto) {
        Finance finance = financeMapper.fromCreateDto(createDto);
        financeRepository.save(finance);
        return ResponseData.successResponse(financeMapper.toDto(finance));
    }

    @Override
    public ResponseData<FinanceResponseDto> update(@NotNull Long id, FinanceUpdateDto updateDto) {
        Finance finance = financeRepository.find(id).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(MessageKeys.FINANCE_NOT_FOUND)));
        finance = financeMapper.fromUpdateDto(updateDto, finance);
        financeRepository.update(finance);
        return ResponseData.successResponse(financeMapper.toDto(finance));
    }

    @Override
    public ResponseData<FinanceResponseDto> delete(Long id) {
        Finance finance = financeRepository.find(id).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(MessageKeys.FINANCE_NOT_FOUND)));
        financeRepository.delete(finance);
        return ResponseData.successResponse(financeMapper.toDto(finance));
    }

    @Override
    public ResponseData<SumOfIncomeCostResponse> sumOfIncomeBetween(DateBetweenRequest request, @NotNull Long id) {
        Double sumOfIncome = financeRepository.getSumOfIncomeBetween(request.getStartDate(), request.getEndDate(), id);
        if (sumOfIncome == null){
            throw new NotFoundException(MessageService.getMessage(MessageKeys.NO_INCOME_AVAILABLE));
        }
        SumOfIncomeCostResponse sumOfIncomeCostResponse = SumOfIncomeCostResponse.builder()
                .sum(sumOfIncome)
                .build();
        return ResponseData.successResponse(sumOfIncomeCostResponse);
    }

    @Override
    public ResponseData<SumOfIncomeCostResponse> sumOfCostBetween(DateBetweenRequest request, @NotNull Long id) {
        Double sumOfCost = financeRepository.getSumOfCostBetween(request.getStartDate(), request.getEndDate(), id);
        if (sumOfCost == null){
            throw new NotFoundException(MessageService.getMessage(MessageKeys.NO_COST_AVAILABLE));
        }
        SumOfIncomeCostResponse sumOfIncomeCostResponse = SumOfIncomeCostResponse.builder()
                .sum(sumOfCost)
                .build();
        return ResponseData.successResponse(sumOfIncomeCostResponse);
    }

    @Override
    public ResponseData<SumOfIncomeCostResponse> sumOfCostOnMonth(Integer month, Integer year, Long id) {
        Double sumOfCost = financeRepository.getSumOfCostOneMonth(month, year, id);
        if (sumOfCost == null) {
            throw new NotFoundException(MessageService.getMessage(MessageKeys.NO_COST_AVAILABLE_THIS_MONTH));
        }
        SumOfIncomeCostResponse response = SumOfIncomeCostResponse.builder()
                .sum(sumOfCost)
                .build();
        return ResponseData.successResponse(response);
    }

    @Override
    public ResponseData<SumOfIncomeCostResponse> sumOfIncomeOnMonth(Integer month, Integer year, Long id) {
        Double sumOfIncome = financeRepository.getSumOfIncomeOneMonth(month, year, id);
        if (sumOfIncome == null) {
            throw new NotFoundException(MessageService.getMessage(MessageKeys.NO_INCOME_AVAILABLE_THIS_MONTH));
        }
        SumOfIncomeCostResponse response = SumOfIncomeCostResponse.builder()
                .sum(sumOfIncome)
                .build();
        return ResponseData.successResponse(response);
    }

    @Override
    public ResponseData<List<FinanceResponseDto>> financeOfCostDay(@NotNull LocalDate date, @NotNull Long id) {
        List<Finance> financeList = financeRepository.getFinanceOfCostDay(date, id).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(MessageKeys.NO_COST_AVAILABLE)));
        return ResponseData.successResponse(financeMapper.toDto(financeList));
    }

    @Override
    public ResponseData<List<FinanceResponseDto>> financeOfIncomeDay(@NotNull LocalDate date, @NotNull Long id) {
        List<Finance> financeList = financeRepository.getFinanceOfIncomeDay(date, id).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(MessageKeys.NO_INCOME_AVAILABLE)));
        return ResponseData.successResponse(financeMapper.toDto(financeList));
    }

    @Override
    public ResponseData<FinanceStatusResponse> financeStatusOfDay(@NotNull LocalDate date, @NotNull Long id) {
        Double status = getStatusDay(date, id);
        IncomeStatus incomeStatus = IncomeStatus.INCOME;
        if (status < 0) {
            incomeStatus = IncomeStatus.COST;
        }
        FinanceStatusResponse response = FinanceStatusResponse.builder()
                .value(status)
                .incomeStatus(incomeStatus)
                .build();
        return ResponseData.successResponse(response);
    }

    @Override
    public ResponseData<FinanceStatusResponse> financeStatusOfMonth(@NotNull Integer year, @NotNull Integer month, @NotNull Long id) {
        Double status = getStatusMonth(month, year, id);
        IncomeStatus incomeStatus = IncomeStatus.INCOME;
        if (status < 0) {
            incomeStatus = IncomeStatus.COST;
        }
        FinanceStatusResponse response = FinanceStatusResponse.builder()
                .value(status)
                .incomeStatus(incomeStatus)
                .build();
        return ResponseData.successResponse(response);
    }

    private Double getStatusDay(LocalDate date, Long id){
        Double cost = financeRepository.getSumOfCostDay(date, id);
        if (cost == null) cost = 0d;
        Double income = financeRepository.getSumOfIncomeDay(date, id);
        if (income == null) income = 0d;
        return income - cost;
    }

    private Double getStatusMonth(Integer month, Integer year, Long id){
        Double cost = financeRepository.getSumOfCostOneMonth(month, year, id);
        if (cost == null) cost = 0d;
        Double income = financeRepository.getSumOfIncomeOneMonth(month, year, id);
        if (income == null) income = 0d;
        return  income - cost;
    }
}
