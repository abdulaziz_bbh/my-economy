package com.example.myeconomy.service.economy;

import com.example.myeconomy.dto.economy.CategoryCreateDto;
import com.example.myeconomy.dto.economy.CategoryResponseDto;
import com.example.myeconomy.dto.economy.CategoryUpdateDto;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.service.GenericService;

import java.util.List;

public interface ICategoryService extends GenericService<CategoryResponseDto, CategoryCreateDto, CategoryUpdateDto, Long> {

    ResponseData<CategoryResponseDto> getByName(String name, Long id);

    ResponseData<List<CategoryResponseDto>> getAllByUserId (Long id);
}
