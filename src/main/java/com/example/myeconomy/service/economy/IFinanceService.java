package com.example.myeconomy.service.economy;

import com.example.myeconomy.dto.economy.*;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.service.GenericService;

import java.time.LocalDate;
import java.util.List;

public interface IFinanceService extends GenericService<FinanceResponseDto, FinanceCreateDto, FinanceUpdateDto, Long> {

    ResponseData<SumOfIncomeCostResponse> sumOfIncomeBetween(DateBetweenRequest dateBetweenRequest, Long id);

    ResponseData<SumOfIncomeCostResponse> sumOfCostBetween(DateBetweenRequest dateBetweenRequest, Long id);

    ResponseData<SumOfIncomeCostResponse> sumOfCostOnMonth(Integer month, Integer year, Long id);

    ResponseData<SumOfIncomeCostResponse> sumOfIncomeOnMonth(Integer month, Integer year, Long id);

    ResponseData<List<FinanceResponseDto>> financeOfCostDay(LocalDate date, Long id);

    ResponseData<List<FinanceResponseDto>> financeOfIncomeDay(LocalDate date, Long id);

    ResponseData<FinanceStatusResponse> financeStatusOfDay(LocalDate date, Long id);

    ResponseData<FinanceStatusResponse> financeStatusOfMonth(Integer year, Integer month, Long id);
}
