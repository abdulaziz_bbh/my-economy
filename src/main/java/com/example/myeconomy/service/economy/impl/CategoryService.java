package com.example.myeconomy.service.economy.impl;

import com.example.myeconomy.domain.economy.Category;
import com.example.myeconomy.dto.economy.CategoryCreateDto;
import com.example.myeconomy.dto.economy.CategoryResponseDto;
import com.example.myeconomy.dto.economy.CategoryUpdateDto;
import com.example.myeconomy.exception.AlreadyExistException;
import com.example.myeconomy.exception.CannotBeNullException;
import com.example.myeconomy.exception.NotFoundException;
import com.example.myeconomy.mapper.economy.CategoryMapper;
import com.example.myeconomy.repository.auth.IUserRepository;
import com.example.myeconomy.repository.economy.ICategoryRepository;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.service.economy.ICategoryService;
import com.example.myeconomy.utils.MessageKeys;
import com.example.myeconomy.utils.MessageService;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class CategoryService implements ICategoryService {

    private final CategoryMapper categoryMapper;
    private final ICategoryRepository categoryRepository;

    @Override
    public ResponseData<CategoryResponseDto> getById(@NotNull Long id) {
        Category category = categoryRepository.find(id).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(MessageKeys.CATEGORY_N0T_FOUND)));
        return ResponseData.successResponse(categoryMapper.toDto(category));
    }

    @Override
    public ResponseData<List<CategoryResponseDto>> getAll() {
        List<Category> categoryList = categoryRepository.findAll();
        return ResponseData.successResponse(categoryMapper.toDto(categoryList));
    }

    @Override
    public ResponseData<CategoryResponseDto> create(CategoryCreateDto createDto) {
        existByCategoryName(createDto.getCategoryName(), createDto.getUserId());
        Category category = categoryMapper.fromCreateDto(createDto);
        categoryRepository.save(category);
        return ResponseData.successResponse(categoryMapper.toDto(category));
    }

    @Override
    public ResponseData<CategoryResponseDto> update(@NotNull Long id, CategoryUpdateDto updateDto) {
        Category category = categoryRepository.find(id).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(MessageKeys.CATEGORY_N0T_FOUND)));
        category = categoryMapper.fromUpdateDto(updateDto, category);
        categoryRepository.update(category);
        return ResponseData.successResponse(categoryMapper.toDto(category));
    }

    @Override
    public ResponseData<CategoryResponseDto> delete(@NotNull Long id) {
        Category category = categoryRepository.find(id).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(MessageKeys.CATEGORY_N0T_FOUND)));
        categoryRepository.delete(category);
        return ResponseData.successResponse(categoryMapper.toDto(category));
    }

    private void existByCategoryName(String name, Long userId) {
        if (categoryRepository.existByCategoryName(name, userId))
            throw new AlreadyExistException(MessageService.getMessage(MessageKeys.CATEGORY_ALREADY_EXIST));
    }

    @Override
    public ResponseData<CategoryResponseDto> getByName(@NotNull String name, @NotNull Long id) {
        if(name.isEmpty() && name.isBlank())
            throw CannotBeNullException.restThrow(MessageService.getMessage(MessageKeys.CATEGORY_NAME_CANNOT_BE_NULL));
        Category category =  categoryRepository.findByName(name, id).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(MessageKeys.CATEGORY_N0T_FOUND)));
        return ResponseData.successResponse(categoryMapper.toDto(category));
    }

    @Override
    public ResponseData<List<CategoryResponseDto>> getAllByUserId(Long id) {
        List<Category> categoryList = categoryRepository.getAllByUserId(id).orElseThrow(
                () -> new NotFoundException(MessageService.getMessage(MessageKeys.NOT_FOUND_ANY_CATEGORY)));
        return ResponseData.successResponse(categoryMapper.toDto(categoryList));
    }
}
