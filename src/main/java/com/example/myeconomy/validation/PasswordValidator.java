package com.example.myeconomy.validation;

import com.example.myeconomy.utils.RestConstant;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator implements ConstraintValidator<ValidPassword, String> {

    public Pattern pattern;
    public Matcher matcher;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return isValidPassword(value);
    }
    private boolean isValidPassword(String password){
        pattern = Pattern.compile(RestConstant.PASSWORD_REGEX);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
