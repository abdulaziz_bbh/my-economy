package com.example.myeconomy.validation;

import com.example.myeconomy.dto.auth.UserCreateDto;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> {
    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        UserCreateDto userCreateDto = (UserCreateDto) value;
        return userCreateDto.getPassword().equals(userCreateDto.getPrePassword());
    }
}
