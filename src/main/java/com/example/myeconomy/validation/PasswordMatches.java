package com.example.myeconomy.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PasswordMatchesValidator.class)
@Documented
public @interface PasswordMatches {

    String message() default  "Kiritilgan parol mos kelmadi :(";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
