package com.example.myeconomy.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.*;
import static java.lang.annotation.ElementType.*;

@Constraint(validatedBy = PasswordValidator.class)
@Target({TYPE, FIELD, ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ValidPassword {

    String message() default  "Parolda kamida bitta katta harf, bitta kichik harf, bitta raqam va !@#$%^&*()_+= bo'lishi kerak";
    Class<?>[] groups() default{};
    Class<? extends Payload>[] payload() default {};
}
