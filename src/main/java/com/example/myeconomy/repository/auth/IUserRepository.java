package com.example.myeconomy.repository.auth;

import com.example.myeconomy.domain.auth.User;
import com.example.myeconomy.repository.BaseRepository;

import java.util.Optional;

public interface IUserRepository extends BaseRepository<User> {

    Optional<User> findByEmail(String email);

    boolean existByEmail(String email);
}
