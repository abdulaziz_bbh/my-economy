package com.example.myeconomy.repository.auth.impl;

import com.example.myeconomy.domain.auth.User;
import com.example.myeconomy.repository.GenericDao;
import com.example.myeconomy.repository.auth.IUserRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UserRepository extends GenericDao<User> implements IUserRepository {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public Optional<User> findByEmail(String email) {
        return  Optional.ofNullable((User) entityManager.createQuery("select t from User t where t.email = :email and t.state <> 'DELETED'")
                .setParameter("email", email)
                .getSingleResult());
    }

    @Override
    public boolean existByEmail(String email) {
        return (boolean) entityManager
                .createQuery("select exists ((select t from User t where  t.email = :email))from User ")
                .setParameter("email", email)
                .getSingleResult();
    }
}
