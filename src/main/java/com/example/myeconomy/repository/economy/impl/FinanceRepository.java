package com.example.myeconomy.repository.economy.impl;

import com.example.myeconomy.domain.economy.Finance;
import com.example.myeconomy.repository.GenericDao;
import com.example.myeconomy.repository.economy.IFinanceRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class FinanceRepository extends GenericDao<Finance> implements IFinanceRepository {

    @PersistenceContext
    private final EntityManager entityManager;


    @Override
    public Double getSumOfIncomeBetween(LocalDate startDate, LocalDate endDate, Long userId) {
        return (Double) entityManager.createQuery("select sum(t.value) from Finance t " +
                "where t.incomeStatus = 'INCOME' and timeAt between :startDate and :endDate and t.user.id = :userId and t.state <> 'DELETED'")
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public Double getSumOfCostBetween(LocalDate startDate, LocalDate endDate, Long userId) {
        return (Double) entityManager.createQuery("select sum(t.value) from Finance t " +
                        "where t.incomeStatus = 'COST' and timeAt between :startDate and :endDate and t.user.id = :userId and t.state <> 'DELETED'")
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .setParameter("userId", userId)
                .getSingleResult();
    }

    @Override
    public Double getSumOfCostOneMonth(Integer month, Integer year, Long userId) {
        return (Double) entityManager.createQuery("select sum (f.value) from Finance f" +
                        " where f.user.id = :userId and " +
                        "extract(month from f.timeAt) = :month and " +
                        "extract(year from f.timeAt) = :year and " +
                        "f.incomeStatus = 'COST' and " +
                        "f.state <> 'DELETED'")
                .setParameter("userId", userId)
                .setParameter("month", month)
                .setParameter("year", year)
                .getSingleResult();
    }

    @Override
    public Double getSumOfIncomeOneMonth(Integer month, Integer year, Long userId) {
        return (Double) entityManager.createQuery("select sum (f.value) from Finance f" +
                        " where f.user.id = :userId and " +
                        "extract(month from f.timeAt) = :month and " +
                        "extract(year from f.timeAt) = :year and " +
                        "f.incomeStatus = 'INCOME' and " +
                        "f.state <> 'DELETED'")
                .setParameter("userId", userId)
                .setParameter("month", month)
                .setParameter("year", year)
                .getSingleResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Optional<List<Finance>> getFinanceOfCostDay(LocalDate date, Long id) {
        return Optional.ofNullable(entityManager.createQuery("select f from Finance f where f.user.id = :id and f.timeAt = :date and f.incomeStatus = 'COST' and  f.state <> 'DELETED'")
                .setParameter("id", id)
                .setParameter("date", date)
                .getResultList());
    }

    @Override
    @SuppressWarnings("unchecked")
    public Optional<List<Finance>> getFinanceOfIncomeDay(LocalDate date, Long id) {
        return Optional.ofNullable(entityManager.createQuery("select f from Finance f where f.user.id = :id and f.timeAt = :date and f.incomeStatus = 'INCOME' and f.state <> 'DELETED'")
                .setParameter("id", id)
                .setParameter("date", date)
                .getResultList());
    }

    @Override
    public Double getSumOfCostDay(LocalDate date, Long id) {
        return (Double) entityManager.createQuery("select sum (f.value) from Finance f where f.timeAt = :date and f.user.id = :id and incomeStatus = 'COST' and f.state <> 'DELETED'")
                .setParameter("date", date)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Double getSumOfIncomeDay(LocalDate date, Long id) {
        return (Double) entityManager.createQuery("select sum (f.value) from Finance f where f.timeAt = :date and f.user.id = :id and incomeStatus = 'INCOME' and f.state <> 'DELETED'")
                .setParameter("date", date)
                .setParameter("id", id)
                .getSingleResult();
    }
}
