package com.example.myeconomy.repository.economy;

import com.example.myeconomy.domain.economy.Finance;
import com.example.myeconomy.repository.BaseRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface IFinanceRepository extends BaseRepository<Finance> {

    Double getSumOfIncomeBetween(LocalDate startDate, LocalDate endDate, Long userId);

    Double getSumOfCostBetween(LocalDate startDate, LocalDate endDate, Long userid);

    Double getSumOfCostOneMonth(Integer month, Integer year, Long userId);

    Double getSumOfIncomeOneMonth(Integer month, Integer year, Long userId);

    Optional<List<Finance>> getFinanceOfCostDay(LocalDate date, Long id);

    Optional<List<Finance>> getFinanceOfIncomeDay(LocalDate date, Long id);

    Double getSumOfCostDay(LocalDate date, Long id);

    Double getSumOfIncomeDay(LocalDate date, Long id);



}
