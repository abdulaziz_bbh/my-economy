package com.example.myeconomy.repository.economy.impl;

import com.example.myeconomy.domain.economy.Category;
import com.example.myeconomy.repository.GenericDao;
import com.example.myeconomy.repository.economy.ICategoryRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class CategoryRepository extends GenericDao<Category> implements ICategoryRepository {

    @PersistenceContext
    private final EntityManager entityManager;

    public CategoryRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public boolean existByCategoryName(String name, Long userId) {
        return (boolean) entityManager.createQuery("select coalesce((select exists ((select 1 from Category t where t.categoryName = :name and t.user.id = :id)) as exist_row from Category t group by exist_row), false )")
                .setParameter("name", name)
                .setParameter("id", userId)
                .getSingleResult();
    }

    @Override
    public Optional<Category> findByName(String name, Long id) {
        try {
            return Optional.ofNullable((Category) entityManager.createQuery("select t from Category t where t.categoryName = :name and t.user.id = : id and t.state <> 'DELETED'")
                    .setParameter("name", name)
                    .setParameter("id", id)
                    .getSingleResult());
        }catch (NoResultException e) {
            return Optional.empty();
        }
    }
    @Override
    @SuppressWarnings("unchecked")
    public Optional<List<Category>> getAllByUserId(Long id) {
        try {
            return Optional.ofNullable(entityManager.createQuery("select t from Category t where t.user.id = :id and t.state <> 'DELETED'")
                    .setParameter("id", id)
                    .getResultList());
        }catch (NoResultException e){
            return Optional.empty();
        }
    }
}