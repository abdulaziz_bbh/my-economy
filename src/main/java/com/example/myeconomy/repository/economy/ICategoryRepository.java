package com.example.myeconomy.repository.economy;

import com.example.myeconomy.domain.economy.Category;
import com.example.myeconomy.repository.BaseRepository;

import java.util.List;
import java.util.Optional;

public interface ICategoryRepository extends BaseRepository<Category> {

    boolean existByCategoryName(String name, Long userId);

    Optional<Category> findByName(String name, Long id);

    Optional<List<Category>> getAllByUserId(Long userId);
}
