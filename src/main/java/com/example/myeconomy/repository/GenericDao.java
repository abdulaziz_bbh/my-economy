package com.example.myeconomy.repository;

import com.example.myeconomy.domain.BaseDomain;
import com.example.myeconomy.enums.State;
import com.google.common.base.Preconditions;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceContext;
import org.hibernate.Session;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

public abstract class GenericDao<E extends BaseDomain> {

    protected Class<E> persistentClass;


    @SuppressWarnings("unchecked")
    public GenericDao(){
        this.persistentClass = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }

    @PersistenceContext
    private EntityManager entityManager;

    public Optional<E> find(Long id){
        try {
        return Optional.ofNullable(entityManager.createQuery(
                "select t from " + persistentClass.getSimpleName() + " t where t.state <> 'DELETED' and t.id = "+ id, persistentClass)
                .getSingleResult());
        }catch (NoResultException e){
            return Optional.empty();
        }
    }

    public List<E> findAll(){
        return entityManager.createQuery("select t from " + persistentClass.getSimpleName() + " t where t.state <> 'DELETED'", persistentClass)
                .getResultList();
    }


    public void save(E entity) {
        Preconditions.checkNotNull(entity);
        entity.setState(State.NEW);
        getCurrentSession().persist(entity);
    }

    public E update(E entity) {
        Preconditions.checkNotNull(entity);
        entity.setState(State.UPDATED);
        getCurrentSession().merge(entity);
        return entity;
    }

    public void delete(E entity) {
        Preconditions.checkNotNull(entity);
        entity.setState(State.DELETED);
        getCurrentSession().persist(entity);
    }

    protected Session getCurrentSession(){
        return entityManager.unwrap(Session.class);
    }

}
