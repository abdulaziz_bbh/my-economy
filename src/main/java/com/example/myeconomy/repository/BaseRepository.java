package com.example.myeconomy.repository;

import com.example.myeconomy.domain.BaseDomain;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;
import java.util.Optional;

@NoRepositoryBean
public interface BaseRepository<E extends BaseDomain> {

    Optional<E> find(Long id);

    List<E> findAll();

    void save(E entity);

    E update(E entity);

    void delete(E entity);



}
