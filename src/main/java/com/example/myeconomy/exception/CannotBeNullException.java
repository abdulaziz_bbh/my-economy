package com.example.myeconomy.exception;

public class CannotBeNullException extends GenericException{
    public CannotBeNullException(String message) {
        super(message);
    }

    public static CannotBeNullException restThrow(String message){
        return new CannotBeNullException(message);
    }
}
