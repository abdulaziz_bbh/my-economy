package com.example.myeconomy.exception;

public class AlreadyExistException extends GenericException{

    public AlreadyExistException(String message) {
        super(message);
    }
}
