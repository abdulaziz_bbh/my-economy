package com.example.myeconomy.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenericException extends RuntimeException{

    private String messageKey;

    public GenericException(String message){
        super(message);
    }

    public GenericException(String message, String messageKey){
        super(message);
        this.messageKey = messageKey;
    }

    public GenericException(String message, Throwable cause) {
        super(message, cause);
    }

    public GenericException(String message, Throwable cause, String messageKey) {
        super(message, cause);
        this.messageKey = messageKey;
    }
}
