package com.example.myeconomy.exception;

public class OTPException extends GenericException{
    public OTPException(String message) {
        super(message);
    }
    public static OTPException restThrow(String message){
        return new OTPException(message);
    }
}
