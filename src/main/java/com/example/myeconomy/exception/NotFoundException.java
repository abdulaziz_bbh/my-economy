package com.example.myeconomy.exception;

public class NotFoundException extends GenericException{


    public NotFoundException(String message) {
        super(message);
    }
}
