package com.example.myeconomy.exception;

import com.example.myeconomy.response.ErrorData;
import com.example.myeconomy.response.ResponseData;
import com.example.myeconomy.utils.MessageKeys;
import com.example.myeconomy.utils.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailSendException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AccountStatusException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class GlobalExceptionHandling{

    Logger logger = LoggerFactory.getLogger(GlobalExceptionHandling.class);

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ResponseData<ErrorData>> handleException(MethodArgumentNotValidException exception, WebRequest request){
        logger.error(exception.getMessage(), exception);
        List<ErrorData> errors = new ArrayList<>();
        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
        fieldErrors.forEach(fieldError -> errors.add(new ErrorData(
                fieldError.getDefaultMessage(),
                fieldError.getField(),
                HttpStatus.BAD_REQUEST.value(),
                LocalDateTime.now(),
                request.getDescription(false))));
        return new ResponseEntity<>(ResponseData.errorResponse(errors), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ResponseData<ErrorData>> handlingException(NotFoundException exception, WebRequest webRequest){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(
                webRequest.getDescription(false),
                exception.getMessage(), HttpStatus.NOT_FOUND.value()),HttpStatus.NOT_FOUND);
    }
    @ExceptionHandler(AlreadyExistException.class)
    public ResponseEntity<ResponseData<ErrorData>> handlingException(AlreadyExistException exception, WebRequest webRequest){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(
                webRequest.getDescription(false),
                exception.getMessage(), HttpStatus.BAD_REQUEST.value()),HttpStatus.BAD_REQUEST);
    }
    @ExceptionHandler(OTPException.class)
    public ResponseEntity<ResponseData<ErrorData>> handlingException(OTPException exception, WebRequest webRequest){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(
                webRequest.getDescription(false),
                exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()),HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(MailSendException.class)
    public ResponseEntity<ResponseData<ErrorData>> handlingException(MailSendException exception, WebRequest webRequest){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(
                webRequest.getDescription(false),
                exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR.value()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    @ExceptionHandler(CannotBeNullException.class)
    public ResponseEntity<ResponseData<ErrorData>> handlingException(CannotBeNullException exception, WebRequest webRequest){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(
                webRequest.getDescription(false),
                exception.getMessage(), HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ResponseData<ErrorData>> handlingException(AccessDeniedException exception, WebRequest webRequest){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(
                webRequest.getDescription(false),
                exception.getMessage(), HttpStatus.FORBIDDEN.value()), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(AccountStatusException.class)
    public ResponseEntity<ResponseData<ErrorData>> handlingException(AccountStatusException exception, WebRequest webRequest){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(
                webRequest.getDescription(false),
                exception.getMessage(), HttpStatus.UNAUTHORIZED.value()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ResponseData<ErrorData>> handlingException(BadCredentialsException exception, WebRequest webRequest){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(
                webRequest.getDescription(false),
                MessageService.getMessage(MessageKeys.BAD_CREDENTIALS_EXCEPTION), HttpStatus.UNAUTHORIZED.value()), HttpStatus.UNAUTHORIZED);
    }

    /**
     * Fallback handles any unhandled exceptions
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseData<ErrorData>> handlingException(Exception exception, WebRequest webRequest){
        logger.error(exception.getMessage(), exception);
        return new ResponseEntity<>(ResponseData.errorResponse(
                webRequest.getDescription(false),
                exception.getMessage(), HttpStatus.UNAUTHORIZED.value()), HttpStatus.UNAUTHORIZED);
    }

}


