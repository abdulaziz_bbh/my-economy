package com.example.myeconomy.utils;

import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.stereotype.Service;
import java.util.Locale;

@Service
public class MessageService {
    public static String getMessage(String key){
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        resourceBundleMessageSource.setBasename("messages/message");
        return resourceBundleMessageSource.getMessage(key, null, Locale.getDefault());
    }

}
