package com.example.myeconomy.utils;

import com.example.myeconomy.domain.auth.User;
import com.example.myeconomy.dto.auth.UserDto;
import com.example.myeconomy.exception.NotFoundException;
import com.example.myeconomy.mapper.auth.UserMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class UserSession {

    private final UserMapper userMapper;

    public UserSession(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public UserDto getUser(){
        UserDto userDto = null;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof User){
                userDto = userMapper.toDto((User) authentication.getPrincipal());
        }
        return userDto;
    }
}
